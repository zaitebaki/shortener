<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Url;

class HistoryController extends SuperController
{
    /**
     * Get history API
     * @param Request $request
     * @return array
     */
    public function getHistory(Request $request)
    {
        $data = $request->all();
        $links = Url::whereIn('token', $data['tokens'])
            ->orderBy('id', 'desc')
            ->get();

        $baseUrl = env('APP_URL');

        $res = $links->map(function ($link) use ($baseUrl) {
            return [
                'srcUrl' => $link->url,
                'shortUrl' => $baseUrl . "/" . $link->token,
                'statisticUrl' => $baseUrl . "/" . $link->token . "/statistic"
            ];
        });

        return $res->toArray();
    }
}




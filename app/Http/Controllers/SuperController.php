<?php

namespace App\Http\Controllers;

class SuperController extends Controller
{
    /**
     * Layout template name.
     * @var string
     */
    protected string $layout;

    /**
     * Page title.
     * @var string
     */
    protected string $title;

    /**
     * Props data.
     * @var array
     */
    protected array $propsData = [];

    /**
     * An array of variables that are passed to the template.
     * @var array
     */
    protected array $vars = [];

    /**
     * Render view.
     * @return object
     */
    protected function renderOutput(): object
    {

        $this->vars = array_add($this->vars, 'title', $this->title);

        if (count($this->propsData) !== 0) {
            $this->vars = array_add($this->vars, 'propsData', $this->propsData);
        }

        return view($this->layout)->with($this->vars);
    }
}

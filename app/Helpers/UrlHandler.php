<?php

namespace App\Helpers;

use App\Statistic;
use App\Url;
use Hashids\Hashids;

class UrlHandler
{
    /**
     * URL validation.
     * @param string $url
     * @return bool
     */
    public static function isValid(string $url): bool
    {
        return !preg_match('/[\r\n]/', $url)
            && preg_match('/\b(?:https?|ftp):\/\/|www\.\S+\.\S+/i', $url);
    }

    /**
     * Get new token.
     * @return string
     */
    public static function getNewToken(): string
    {
        $milliseconds = (int) round(microtime(true) * 1000);
        $hashids = new Hashids($milliseconds, 5);

        return $hashids->encode(1);
    }

    /**
     * Save url.
     * @param string $url
     * @param string $token
     * @param string|null $date
     *
     * @return void
     */
    public static function saveUrl(string $url, string $token, ?string $date): void
    {
        $newUrl = [
            'url' => $url,
            'token' => $token,
            'lifetime' => $date,
        ];

        $newUrl = new Url($newUrl);
        $newUrl->save();
    }

    /**
     * Is link active
     * @param ?string $lifeTimeUrl
     *
     * @return bool
     */
    public static function isActive(?string $lifeTimeUrl): bool
    {
        return ($lifeTimeUrl === null || strtotime($lifeTimeUrl) + 86400 >= time());
    }


    /**
     * Save statistic.
     * @param Url $urlModel
     * @param string $date
     * @param string|null $country
     * @param string|null $city
     * @return void
     */
    public static function saveStatistic(
        Url $urlModel,
        string $date,
        string $country = null,
        string $city = null
    ): void {
        $newStatistic = [
            'date_time' => $date,
            'country' => $country,
            'city' => $city,
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        ];

        $newStatistic = new Statistic($newStatistic);
        $urlModel->statistic()->save($newStatistic);
    }
}

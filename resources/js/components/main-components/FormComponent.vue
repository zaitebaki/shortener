<template>
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-md-8 py-5">
        <h1 class="font-weight-bold">
          {{ content.pageCaption }}
        </h1>
        <hr>
        <form
          id="editForm"
          method="post"
          :action="data.indexRoute"
        >
          <input
            type="hidden"
            name="_token"
            :value="csrf"
          >
          <input
            type="hidden"
            name="newLink"
            :value="data.shortLink"
          >
          <div
            v-if="data.shortLink"
            class="form-group clipboard-cont"
          >
            <label
              class="text-success"
              for="shortLink"
            >{{
              content.shortLinkCaption
            }}</label>
            <input
              id="shortLink"
              type="text"
              class="form-control"
              name="shortLink"
              :value="data.shortLink"
              readonly
              @click="copyText($event)"
            >
          </div>
          <div
            v-if="data.statisticLink"
            class="form-group clipboard-cont"
          >
            <label
              class="text-success"
              for="statisticLink"
            >{{
              content.statisticLinkCaption
            }}</label>
            <input
              id="statisticLink"
              type="text"
              class="form-control"
              name="statisticLink"
              :value="data.statisticLink"
              readonly
              @click="copyText($event)"
            >
          </div>
          <div class="form-group">
            <label
              v-if="data.shortLink === null || data.error !== null"
              for="urlInputTextarea"
            >{{
              content.inputUrlCaption
            }}</label>
            <label
              v-if="data.shortLink !== null && data.error === null"
              for="urlInputTextarea"
            >{{
              content.srcUrlCaption
            }}</label>
            <textarea
              id="urlInputTextarea"
              class="form-control"
              name="userUrl"
              rows="3"
              :value="data.userLink"
              required
              :readonly="data.shortLink !== null && data.error === null"
            />
            <small
              v-if="data.error"
              class="form-text text-danger"
            >{{ data.error }}</small>
          </div>
          <button
            v-if="data.shortLink === null || data.error !== null"
            type="submit"
            class="btn btn-primary"
          >
            {{ content.buttonCaption }}
          </button>
          <button
            v-if="data.shortLink !== null && data.error === null"
            type="submit"
            class="btn btn-primary"
          >
            {{ content.newButtonCaption }}
          </button>

          <div
            v-if="data.shortLink === null || data.error !== null"
            class="form-group py-4"
          >
            <label for="datePicker">{{
              content.datePickerCaption
            }}</label>
            <datepicker
              :value="vmodelDate"
              :disabled-dates="disabledFn"
              name="dataPicker"
              placeholder=" указать дату"
            />
          </div>
        </form>
      </div>
    </div>
  </div>
</template>

http://127.0.0.1:8000/KmlWR/statistic

<script>
import Datepicker from 'vuejs-datepicker';

export default {
    components: {
        Datepicker,
    },
    props: {
        data: {
            type: Object,
            required: true,
        },
        content: {
            type: Object,
            required: true
        },
    },
    data() {
        return {
            csrf: document
                .querySelector('meta[name="csrf-token"]')
                .getAttribute('content'),
            disabledFn: {
                customPredictor(date) {
                    const today = new Date();
                    today.setHours(0, 0, 0, 0);
                    if (date < today) {
                        return true;
                    }
                },
            },
            vmodelDate: null,
        };
    },
    computed: {
        isLinkLoaded() {
            return this.data.shortLink;
        },
    },
    mounted() {
        // save new token to Local Storage
        const maxLinksInCache = 10;
        if (this.isLinkLoaded) {
            const storageLinks = localStorage.getItem('links');
            const links = storageLinks ? JSON.parse(storageLinks) : [];

            const token = this.getToken(this.data.shortLink);
            if (!links.includes(token)) {
                links.unshift(token);
            }

            if (links.length > maxLinksInCache) {
                links.splice(maxLinksInCache);
            }

            localStorage.setItem('links', JSON.stringify(links));
        }

        if (this.data.error) {
            console.log(this.data.error)
        }

    },
    methods: {
        getToken(link) {
            const parts = link.split('/');
            return parts[parts.length - 1] || '';
        },
        copyText(event) {
            // copy to clipboard
            const input = document.createElement('textarea');
            input.value = event.target.value;
            document.body.appendChild(input);
            input.select();
            document.execCommand('copy');
            document.body.removeChild(input);

             //create success message
            const elements = document.querySelectorAll(".copy-msg");
            elements.forEach((element) => {
              element.remove();
            });
            const div = document.createElement('div');
            div.classList.add('copy-msg');
            div.setAttribute('role', 'alert');
            div.textContent = this.content['onCopiedMsg'];
            const id = event.target.id;
            const parent = document.getElementById(id).parentNode;
            parent.appendChild(div);
            setTimeout(() => {
                div.remove();
            }, 2500);
        }
    },
};
</script>

<style>
    .clipboard-cont {
        position: relative;
    }
    .copy-msg {
        background-color: #d7f3e3;
        border-color: #c7eed8;
        border-radius: 5px;
        box-sizing: border-box;
        color: #1d643b;
        height: 30px;
        line-height: 30px;
        position: absolute;
        right: 0;
        text-align: center;
        top: 72px;
        width: 125px;
    }
</style>


